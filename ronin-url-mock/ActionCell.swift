//
//  ActionCell.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 08/03/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit

class ActionCell: UITableViewCell {
    
    @IBOutlet weak var actionButton: UIButton!
    
    var action: URLActions!
    
    weak var ownerViewController: FirstViewController!
    
    func setupCell(ownerViewController: FirstViewController) {
        self.ownerViewController = ownerViewController
    }
    
    func perform() {
        if self.ownerViewController.addAuth {
            self.action.perform(self.ownerViewController.userID, URLValidity: defaultValidity)
        } else {
            self.action.perform(nil, URLValidity: nil)
        }
    }
    
    @IBAction func actionButtonAction(sender: UIButton) {
        switch action! {
        case .Action:
            RPAlertView.show(
                style: RPAlertViewStyle.Question,
                title: "Action ID",
                subTitle: "Enter action id to show",
                closeButtonTitle: "Cancel") { alert in
                    let textField = alert.addTextField("Action Id")
                    alert.addButton("Ok") {
                        self.action = URLActions.Action(actionId: Int(textField.text ?? "0")!)
                        self.perform()
                    }
            }
        case .CategoryActions:
            RPAlertView.show(
                style: RPAlertViewStyle.Question,
                title: "Category ID",
                subTitle: "Enter category id to show",
                closeButtonTitle: "Cancel") { alert in
                    let textField = alert.addTextField("Category Id")
                    alert.addButton("Ok") {
                        if let categoryId = Int(textField.text ?? "0") {
                            self.action = URLActions.CategoryActions(categoryId: categoryId)
                        } else {
                            self.action = URLActions.Categories
                        }
                        self.perform()
                    }
            }
        case .VendorActions:
            RPAlertView.show(
                style: RPAlertViewStyle.Question,
                title: "Vendor ID",
                subTitle: "Enter vendor id to show",
                closeButtonTitle: "Cancel") { alert in
                    let textField = alert.addTextField("Vendor Id")
                    alert.addButton("Ok") {
                        self.action = URLActions.VendorActions(vendorId: Int(textField.text ?? "0")!)
                        self.perform()
                    }
            }
        default:
            self.perform()
        }
    }
    
}
