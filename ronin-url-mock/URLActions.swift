//
//  URLActions.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 18/02/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit

enum URLActions {
    case OpenApp
    case Actions
    case Action(actionId: Int)
    case Categories
    case CategoryActions(categoryId: Int)
    case Vendor
    case VendorActions(vendorId: Int)
    case Map
    case Balance
    case Profile
    case Social
    case Targets
    case Rewards
    case Games
    case Bookmarks
    case Favourites
    case Invites
    case Settings
    case Tariffs
    case Contacts
    case Registration
    case Transfer

    case AfterLogin(authenticate: String, userID: String, validity: Int)
    
    var title: String {
        switch self {
        case .OpenApp: return "Open application"
        case .Actions: return "Actions feed"
        case .Action: return "Action"
        case .Categories: return "Category list"
        case .CategoryActions: return "Actions within category"
        case .Vendor: return "Vendor"
        case .VendorActions: return "Vendor actions"
        case .Map: return "Map"
        case .Balance: return "Balance"
        case .Profile: return "Profile"
        case .Social: return "Social"
        case .Targets: return "Targets"
        case .Rewards: return "Rewards"
        case .Games: return "Games"
        case .Bookmarks: return "Bookmarks"
        case .Favourites: return "Favourites"
        case .Invites: return "Invites"
        case .Settings: return "Settings"
        case .Tariffs: return "Tariffs"
        case .Contacts: return "Contacts"
        case .Registration: return "Registration"
        case .Transfer: return "Transfer"
        case .Transfer: return "After Login"
        default: return "?"
        }
    }
    
    var currentScheme: String {
        return Applications[currentApplicationIndex.appIndex].items[currentApplicationIndex.kindIndex].urlScheme
    }
    
    var currentHost: String {
        switch self {
        case .OpenApp: return "open"
        case .Actions: return "action"
        case .Action: return "action"
        case .Categories: return "category"
        case .CategoryActions: return "category"
        case Vendor: return "vendor"
        case VendorActions: return "vendor"
        case Map: return "map"
        case Balance: return "balance"
        case Profile: return "profile"
        case Social: return "social"
        case Targets: return "targets"
        case Rewards: return "rewards"
        case Games: return "games"
        case Bookmarks: return "bookmarks"
        case Favourites: return "favourites"
        case Invites: return "invites"
        case Settings: return "settings"
        case Tariffs: return "tariffs"
        case Contacts: return "contacts"
        case Registration: return "registration"
        case Transfer: return "transfer"

        case AfterLogin: return "afterlogin"
        }
    }
    
    static func actionWithIndex(index: Int) -> URLActions? {
        switch index {
        case  0: return URLActions.OpenApp
        case  1: return URLActions.Actions
        case  2: return URLActions.Action(actionId: 0)
        case  3: return URLActions.Categories
        case  4: return URLActions.CategoryActions(categoryId: 0)
        case  5: return URLActions.Vendor
        case  6: return URLActions.VendorActions(vendorId: 0)
        case  7: return URLActions.Map
        case  8: return URLActions.Balance
        case  9: return URLActions.Profile
        case 10: return URLActions.Social
        case 11: return URLActions.Targets
        case 12: return URLActions.Rewards
        case 13: return URLActions.Games
        case 14: return URLActions.Bookmarks
        case 15: return URLActions.Favourites
        case 16: return URLActions.Invites
        case 17: return URLActions.Settings
        case 18: return URLActions.Tariffs
        case 19: return URLActions.Contacts
        case 20: return URLActions.Registration
        case 21: return URLActions.Transfer
        default: return nil
        }
    }
    
    func perform(URLUserID: String?, URLValidity: Int?) {
        var query: [String: String] = [:]
        
        switch self {
        case Action(let actionId):
            query = ["id": "\(actionId)"]
        case CategoryActions(let categoryId):
            query = ["id": "\(categoryId)"]
        case VendorActions(let vendorId):
            query = ["id": "\(vendorId)"]
        case let .AfterLogin(authenticate, userID, validity):
            query["authenticate"] = authenticate
            query["identifier"] = userID
            query["validity"] = String(validity)
        default: break
        }
        
        if let userID = URLUserID {
            query["identifier"] = userID
        }
        
        if let validity = URLValidity {
            query["validity"] = String(validity)
        }
        
        if let url = NSURL.makeURLWithScheme(currentScheme, host: currentHost, path: nil, query: query) {
            url.showURLWithTitle(title) {
                if !UIApplication.sharedApplication().openURL(url) {
                    url.errorOpenURL()
                }
            }
        }
    }
    
}

