//
//  AuthorizationCell.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 08/03/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit

class AuthorizationCell: UITableViewCell {
    
    @IBOutlet weak var successButton: UIButton!
    @IBOutlet weak var failedButton: UIButton!
    @IBOutlet weak var cancelledButton: UIButton!
    
    weak var ownerViewController: FirstViewController!
    
    func setupCell(ownerViewController: FirstViewController) {
        self.ownerViewController = ownerViewController
    }
    
    @IBAction func successButtonAction(sender: AnyObject) {
        let action = URLActions.AfterLogin(authenticate: "SUCCESSFUL", userID: ownerViewController.userID, validity: defaultValidity)
        action.perform(nil, URLValidity: nil)
    }
    
    @IBAction func failedButtonAction(sender: AnyObject) {
        let action = URLActions.AfterLogin(authenticate: "FAILED", userID: ownerViewController.userID, validity: defaultValidity)
        action.perform(nil, URLValidity: nil)
    }
    
    @IBAction func cancelledButtonAction(sender: AnyObject) {
        let action = URLActions.AfterLogin(authenticate: "CANCELLED", userID: ownerViewController.userID, validity: defaultValidity)
        action.perform(nil, URLValidity: nil)
    }
    
}
