//
//  SecondViewController.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 15/02/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit
import BEMCheckBox

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SecondViewController: UITableViewDataSource, UITableViewDelegate {
 
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return Applications.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let app = Applications[indexPath.section]
        let info = app.items[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ProviderCell", forIndexPath: indexPath) as! ProviderCell
        cell.appTitleLabel.text = info.kind.stringValue
        cell.checkBox.animationDuration = 0.2
        cell.checkBox.on = currentApplicationIndex.appIndex == indexPath.section && currentApplicationIndex.kindIndex == indexPath.row
        cell.checkBox.onFillColor = cell.checkBox.onTintColor.colorWithAlphaComponent(0.5)
        cell.checkBox.onCheckColor = UIColor.whiteColor()
        cell.checkBox.userInteractionEnabled = false
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let app = Applications[section]
        return app.appName
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let oldIndexPath = NSIndexPath(forRow: currentApplicationIndex.kindIndex, inSection: currentApplicationIndex.appIndex)
        currentApplicationIndex.appIndex = indexPath.section
        currentApplicationIndex.kindIndex = indexPath.row
        tableView.reloadRowsAtIndexPaths([oldIndexPath, indexPath], withRowAnimation: .Automatic)
    }
    
}

class ProviderCell: UITableViewCell {
    
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var appTitleLabel: UILabel!
    
}