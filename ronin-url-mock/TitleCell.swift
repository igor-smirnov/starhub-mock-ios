//
//  TitleCell.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 08/03/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {
    
    @IBOutlet weak var currentAppLabel: UILabel!
    
}
