//
//  OptionsCell.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 08/03/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import BEMCheckBox

class OptionsCell: UITableViewCell {
    
    @IBOutlet weak var userIDTextField: JVFloatLabeledTextField!
    @IBOutlet weak var addAuthLabel: UILabel!
    @IBOutlet weak var addAuthCheckBox: BEMCheckBox!
    
    weak var ownerViewController: FirstViewController!
    
    var tapGestureRecognizer: UITapGestureRecognizer!
    
    func setupCell(ownerViewController: FirstViewController) {
        self.ownerViewController = ownerViewController
        userIDTextField.delegate = self
        userIDTextField.text = ownerViewController.userID
        addAuthCheckBox.on = ownerViewController.addAuth
        addAuthCheckBox.delegate = self
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapGestureRecognized:"))
        addAuthLabel.addGestureRecognizer(tapGestureRecognizer)
        addAuthLabel.userInteractionEnabled = true
    }
    
    func tapGestureRecognized(sender: AnyObject) {
        ownerViewController.addAuth = !ownerViewController.addAuth
        addAuthCheckBox.setOn(ownerViewController.addAuth, animated: true)
    }
    
}

extension OptionsCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        userIDTextField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let newString = ((textField.text ?? "") as NSString).stringByReplacingCharactersInRange(range, withString: string)
        ownerViewController.userID = newString
        return true
    }
    
}

extension OptionsCell: BEMCheckBoxDelegate {
    
    func didTapCheckBox(checkBox: BEMCheckBox) {
        ownerViewController.addAuth = checkBox.on
    }

    
}