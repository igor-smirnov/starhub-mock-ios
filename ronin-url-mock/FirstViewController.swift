//
//  FirstViewController.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 15/02/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit

let defaultValidity = 60

class FirstViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var addAuth = false
    var userID = "ABCD1234"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension FirstViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return 1
        case 2: return 1
        case 3: return 22
        default: return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        func setupButton(button: UIButton) {
            button.layer.borderColor = button.tintColor.CGColor
            button.layer.borderWidth = 1.0
            button.layer.cornerRadius = 10.0
            button.layer.backgroundColor = button.tintColor.colorWithAlphaComponent(0.5).CGColor
            button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        }
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("TitleCell", forIndexPath: indexPath) as! TitleCell
            let app = Applications[currentApplicationIndex.appIndex]
            let item = app.items[currentApplicationIndex.kindIndex]
            cell.currentAppLabel.text = "\(app.appName) \(item.kind.stringValue) scheme: \(item.urlScheme)://"
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("AuthorizationCell", forIndexPath: indexPath) as! AuthorizationCell
            cell.setupCell(self)
            setupButton(cell.successButton)
            setupButton(cell.failedButton)
            setupButton(cell.cancelledButton)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("OptionsCell", forIndexPath: indexPath) as! OptionsCell
            cell.setupCell(self)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("ActionCell", forIndexPath: indexPath) as! ActionCell
            cell.setupCell(self)
            if let action = URLActions.actionWithIndex(indexPath.row) {
                cell.action = action
                cell.actionButton.setTitle(cell.action!.title, forState: .Normal)
            }
            setupButton(cell.actionButton)
            return cell
        default: return tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        }
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Current app"
        case 1: return "Authorization"
        case 2: return "User Info"
        case 3: return "URL Actions"
        default: return nil
        }
    }
    
}
