//
//  Extensions.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 15/02/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit

extension UILabel {
    
    func calculateHeight(width: CGFloat = 0.0) -> CGFloat {
        #if !JURONGPOINT_VVEXCHANGE
            let textSize = CGSizeMake(bounds.width, 10000.0)
            let attrs = [NSFontAttributeName: font]
            if let text = text {
                return text.boundingRectWithSize(textSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attrs, context: nil).height
            } else {
                return 0.0
            }
        #else
            let textSize = CGSizeMake(bounds.width, 10000.0)
            let attrs = [NSFontAttributeName: font]
            if let text = text {
                return text.boundingRectWithSize(textSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attrs, context: nil).height
            } else {
                return 0.0
            }
        #endif
    }
    
    func calculateWidth() -> CGFloat {
        #if !JURONGPOINT_VVEXCHANGE
            let textSize = CGSizeMake(10000.0, bounds.height)
            let attrs = [NSFontAttributeName: font]
            if let text = text {
                return text.boundingRectWithSize(textSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attrs, context: nil).width
            } else {
                return 0.0
            }
        #else
            let textSize = CGSizeMake(10000.0, bounds.height)
            let attrs = [NSFontAttributeName: font]
            if let text = text {
                return text.boundingRectWithSize(textSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attrs, context: nil).width
            } else {
                return 0.0
            }
        #endif
    }
    
}

extension UIColor {
    
    class func colorWithInt(red red: UInt, green: UInt, blue: UInt, alpha: UInt) -> UIColor {
        let r = CGFloat(red) / 255.0
        let g = CGFloat(green) / 255.0
        let b = CGFloat(blue) / 255.0
        let a = CGFloat(alpha) / 255.0
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
    class func colorWith(hex hex: UInt) -> UIColor {
        let red = hex % 0x100
        let green = (hex >> 8) % 0x100
        let blue = (hex >> 16) % 0x100
        let alpha = (hex >> 24) % 0x100
        return UIColor.colorWithInt(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}

