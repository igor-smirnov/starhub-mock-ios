//
//  RPAlertView.swift
//  RoninB2C
//
//  Copyright (c) 2015 Ronin Project. All rights reserved.
//

import UIKit

// Pop Up Styles
enum RPAlertViewStyle {
    case Question, Success, Error, Notice, Warning, Info, Edit
}

// Action Types
enum RPAlertActionType {
    case None, Selector, Closure
}

// Button sub-class
class RPAlertButton: UIButton {
    var actionType = RPAlertActionType.None
    var target:AnyObject!
    var selector:Selector!
    var action:(()->Void)!
    var color: UIColor?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
    }
}

typealias RPAlertCustomize = (alert: RPAlertView) -> Void
typealias RPAlertViewWillHideEvent = (Void) -> Void

final class RPAlertLabel: UIView {
    
    let label = UILabel()
    
    func createObjects() {
        label.textAlignment = .Center
        label.numberOfLines = 0
        label.lineBreakMode = .ByWordWrapping
        addSubview(label)
    }
    
    override func layoutSubviews() {
        label.frame = CGRectMake(0, 0, frame.width, frame.height)
        frame.size.height = label.calculateHeight()
        super.layoutSubviews()
        label.frame = CGRectMake(0, 0, frame.width, frame.height)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createObjects()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createObjects()
    }
    
}

// The Main Class
class RPAlertView: UIViewController {
    let kDefaultShadowOpacity: CGFloat = 0.7
    let kCircleTopPosition: CGFloat = -12.0
    let kCircleBackgroundTopPosition: CGFloat = -15.0
    var kCircleHeight: CGFloat = 56.0
    var kCircleHeightBackground: CGFloat = 62.0
    var kCircleIconHeight: CGFloat = 20.0
    let kTitleTop:CGFloat = 24.0
    let kTitleHeight:CGFloat = 40.0
    var kWindowWidth: CGFloat = 0.0
    var kWindowInsets: CGFloat = 60.0
    var kButtonHeight: CGFloat = 35.0
    var kTextFieldHeight: CGFloat = 30.0
    
    var textEditFont = UIFont.systemFontOfSize(16)
    var buttonFont = UIFont.systemFontOfSize(16)
    var titleLabelFont = UIFont.systemFontOfSize(24)
    var textFont = UIFont.systemFontOfSize(16)
    
    // Members declaration
    var baseView = UIView()
    var viewColor = UIColor()
    var pressBrightnessFactor = 0.85
    var viewStyle: RPAlertViewStyle = .Success
    var titleLabel = RPAlertLabel()
    var textLabel = RPAlertLabel()
    var contentView = UIView()
    var circleBG = UIView()
    var circleView = UIView()
    var circleIconImageView = UIImageView()
    var circleIconImageViewFrame: CGRect!
    var durationTimer: NSTimer!
    var alertViewWillHide: RPAlertViewWillHideEvent?
    var wasClosed = false
    var views = [UIView]()
    var isHidden = false
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)

        // hide any keyboard shown
        UIApplication.sharedApplication().sendAction(Selector("resignFirstResponder"), to: nil, from: nil, forEvent: nil)        
        
        // Set up main view
        view.frame = UIApplication.sharedApplication().keyWindow!.bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleWidth]
        view.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:kDefaultShadowOpacity)
        view.addSubview(baseView)
        baseView.frame = view.frame
        baseView.addSubview(contentView)
        
        // Content View
        contentView.backgroundColor = UIColor(white:1, alpha:1)
        contentView.layer.cornerRadius = 5
        contentView.layer.masksToBounds = true
        contentView.layer.borderWidth = 0.5
        // Circle View
        circleBG.backgroundColor = UIColor.whiteColor()
        baseView.addSubview(circleBG)
        circleBG.addSubview(circleView)
        circleView.addSubview(circleIconImageView)
        
        // Title
        titleLabel.label.numberOfLines = 1
        titleLabel.label.textAlignment = .Center
        
        addView(titleLabel)
        //labelTitle.frame = CGRect(x:12, y:kTitleTop, width: kWindowWidth - 24, height:kTitleHeight)
        
        // View text
        textLabel.label.textAlignment = .Center
        addView(textLabel)
        
        addView(UIView(frame: CGRectMake(0, 0, 100, 10)))
        
        // Colours
        contentView.backgroundColor = UIColor.colorWith(hex: 0xFFFFFFFF)
        titleLabel.label.textColor = UIColor.colorWith(hex: 0xFF4D4D4D)
        textLabel.label.textColor = UIColor.colorWith(hex: 0xFF4D4D4D)
        contentView.layer.borderColor = UIColor.colorWith(hex: 0xFFCCCCCC).CGColor
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let sz = UIApplication.sharedApplication().keyWindow!.frame.size
        if sz.width < sz.height {
            kWindowWidth = sz.width - kWindowInsets
        } else {
            kWindowWidth = sz.height - kWindowInsets
        }
        
        circleBG.frame = CGRectMake(0, 0, kCircleHeightBackground, kCircleHeightBackground)
        circleBG.layer.cornerRadius = circleBG.frame.size.height / 2

        var x = (kCircleHeightBackground - kCircleHeight) / 2
        circleView.frame = CGRect(x: x, y: x, width: kCircleHeight, height: kCircleHeight)
        circleView.layer.cornerRadius = circleView.frame.size.height / 2
        x = (kCircleHeight - kCircleIconHeight) / 2
        
        if viewStyle == .Question {
            circleIconImageView.frame = CGRect(x: x - 2.0, y: x - 5.0, width: kCircleIconHeight + 4.0, height: kCircleIconHeight + 10.0)
        } else {
            circleIconImageView.frame = CGRect(x: x, y: x, width: kCircleIconHeight, height: kCircleIconHeight)
        }

        // Set background frame
        view.frame.size = sz
        // Set frames
        var y = kCircleHeight / 1.5
        
        for item in views {
            if let v = item as? RPAlertButton {
                v.frame = CGRect(x:12, y:y, width:kWindowWidth - 24, height: kButtonHeight)
                v.layer.cornerRadius = 3
                y += kButtonHeight + 10.0
            } else {
                if let v = item as? UITextField {
                    v.frame = CGRect(x:12, y:y, width:kWindowWidth - 24, height: kTextFieldHeight)
                    v.layer.cornerRadius = 3
                    y += kTextFieldHeight + 10.0
                } else {
                    item.frame = CGRect(x: 12, y: y, width: kWindowWidth - 24, height: item.bounds.height)
                    item.layoutSubviews()
                    if item != titleLabel {
                        y += item.bounds.height + 5
                    } else {
                        y += item.bounds.height + 5
                    }
                }
            }
        }
        //y += 14.0 !!!
        contentView.frame = CGRect(
            x:(sz.width - kWindowWidth) / 2,
            y:(sz.height - y) / 2,
            width:kWindowWidth,
            height:y + 8
        )
        circleBG.frame = CGRect(
            x:(sz.width - kCircleHeightBackground) / 2,
            y:(sz.height - y - (kCircleHeight)) / 2,
            width:kCircleHeightBackground,
            height:kCircleHeightBackground
        )
    }
    
    override func touchesEnded(touches:Set<UITouch>, withEvent event:UIEvent?) {
        if let count = event?.touchesForView(view)?.count where count > 0 {
            view.endEditing(true)
        }
    }
    
    func addView(view: UIView)->UIView {
        // Add button
        view.layer.masksToBounds = true
        contentView.addSubview(view)
        views.append(view)
        return view
    }
    
    
    func addTextField(title:String?=nil)->UITextField {
        // Add text field
        let txt = UITextField()
        txt.borderStyle = UITextBorderStyle.RoundedRect
        txt.font = textEditFont
        txt.autocapitalizationType = UITextAutocapitalizationType.Words
        txt.clearButtonMode = UITextFieldViewMode.WhileEditing
        txt.layer.masksToBounds = true
        txt.layer.borderWidth = 1.0
        if title != nil {
            txt.placeholder = title!
        }
        contentView.addSubview(txt)
        views.append(txt)
        return txt
    }
    
    func addButton(title: String, action: () -> Void) -> RPAlertButton {
        let btn = addButton(title)
        btn.actionType = RPAlertActionType.Closure
        btn.action = action
        return btn
    }
    
    func addButton(title: String, target: AnyObject, selector: Selector)->RPAlertButton {
        let btn = addButton(title)
        btn.actionType = RPAlertActionType.Selector
        btn.target = target
        btn.selector = selector
        return btn
    }
    
    private func addButton(title: String)->RPAlertButton {
        // Add button
        let btn = RPAlertButton()
        btn.layer.masksToBounds = true
        btn.setTitle(title, forState: .Normal)
        btn.titleLabel?.font = buttonFont
        btn.addTarget(self, action:Selector("buttonTapped:"), forControlEvents:.TouchUpInside)
        btn.addTarget(self, action:Selector("buttonTapDown:"), forControlEvents:[.TouchDown, .TouchDragEnter])
        btn.addTarget(self, action:Selector("buttonRelease:"), forControlEvents:[.TouchUpInside, .TouchUpOutside, .TouchCancel, .TouchDragOutside] )
        contentView.addSubview(btn)
        views.append(btn)
        return btn
    }
    
    func buttonTapped(btn: RPAlertButton) {
        if btn.actionType == RPAlertActionType.Closure {
            btn.action()
        } else if btn.actionType == RPAlertActionType.Selector {
            let ctrl = UIControl()
            ctrl.sendAction(btn.selector, to:btn.target, forEvent:nil)
            if btn.selector == Selector("hideView") {
                return
            }
        } else {
            #if RONIN || DEBUG
                RPLog.log("Unknow action type for button", level: .Error)
            #endif
        }
        hideView()
    }
    
    func buttonTapDown(btn: RPAlertButton) {
        var hue : CGFloat = 0
        var saturation : CGFloat = 0
        var brightness : CGFloat = 0
        var alpha : CGFloat = 0
        btn.backgroundColor?.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        brightness = brightness * CGFloat(pressBrightnessFactor)
        btn.backgroundColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
    }
    
    func buttonRelease(btn: RPAlertButton) {
        btn.backgroundColor = btn.color ?? viewColor
    }
    
    func show(style: RPAlertViewStyle, title: String, subTitle: String, closeButtonTitle: String? = nil, duration: NSTimeInterval = 0.0, customize: RPAlertCustomize? = nil) {

        guard let rv = UIApplication.sharedApplication().keyWindow?.subviews.first else { return }
        titleLabel.label.font = titleLabelFont
        textLabel.label.font = textFont

        view.alpha = 0
        rv.addSubview(view)
        view.frame = rv.bounds
        baseView.frame = rv.bounds
        
        // Alert colour/icon
        viewColor = UIColor()
        var iconImage: UIImage
        
        viewStyle = style
        
        // Icon style
        switch style {
        case .Question:
            viewColor = UIColor.colorWith(hex: 0xFF73B522)
            iconImage = RPAlertViewStyleKit.imageOfQuestionmark
            
        case .Success:
            #if MTS
                viewColor = UIColor.colorWith(hex: 0xFFBF6628)
            #else
                viewColor = UIColor.colorWith(hex: 0xFF73B522)
            #endif
            iconImage = RPAlertViewStyleKit.imageOfCheckmark
            
        case .Error:
            viewColor = UIColor.colorWith(hex: 0xFF2D27C1)
            iconImage = RPAlertViewStyleKit.imageOfCross
            
        case .Notice:
            viewColor = UIColor.colorWith(hex: 0xFF757372)
            iconImage = RPAlertViewStyleKit.imageOfNotice
            
        case .Warning:
            viewColor = UIColor.colorWith(hex: 0xFF10D1FF)
            iconImage = RPAlertViewStyleKit.imageOfWarning
            
        case .Info:
            viewColor = UIColor.colorWith(hex: 0xFFBF6628)
            iconImage = RPAlertViewStyleKit.imageOfInfo
            
        case .Edit:
            viewColor = UIColor.colorWith(hex: 0xFFFF29A4)
            iconImage = RPAlertViewStyleKit.imageOfEdit
        }
        
        // Title
        if !title.isEmpty {
            titleLabel.label.text = title
            titleLabel.label.textColor = viewColor
        }
        
        // Subtitle
        if !subTitle.isEmpty {
            textLabel.label.text = subTitle
        }
        
        customize?(alert: self)
        
        // Done button
        if let txt = closeButtonTitle {
            addButton(txt) {
                self.wasClosed = true
                self.hideView()
            }
        }
        
        // Alert view colour and images
        self.circleView.backgroundColor = viewColor
        self.circleIconImageView.image  = iconImage
        
        for item in views {
            if let v = item as? UITextField {
                v.layer.borderColor = viewColor.CGColor
            }
            if let v = item as? RPAlertButton {
                v.backgroundColor = v.color ?? viewColor
                if style == RPAlertViewStyle.Warning {
                    v.setTitleColor(UIColor.blackColor(), forState:UIControlState.Normal)
                }
            }
        }
        
        // Adding duration
        if duration > 0 {
            durationTimer?.invalidate()
            durationTimer = NSTimer.scheduledTimerWithTimeInterval(duration, target: self, selector: Selector("hideView"), userInfo: nil, repeats: false)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide", name: UIKeyboardWillHideNotification, object: nil)
        
        // Animate in the alert view
        self.baseView.frame.origin.y = -400
        UIView.animateWithDuration(0.3, animations: {
            self.baseView.center.y = rv.center.y + 50
            self.view.alpha = 1
            }, completion: { finished in
                UIView.animateWithDuration(0.3, animations: {
                    self.baseView.center = rv.center
                    
                    for view in self.views {
                        if let v = view as? UITextField where v.enabled {
                            v.becomeFirstResponder()
                            break
                        }
                    }
                })
        })
    }
    // showTitle(view, title, subTitle, style)
    class func show(style style: RPAlertViewStyle, title: String, subTitle: String, closeButtonTitle: String? = nil, duration: NSTimeInterval = 0.0, customize: RPAlertCustomize? = nil) -> RPAlertView {
        let alert = RPAlertView()
        alert.show(style, title: title, subTitle: subTitle, closeButtonTitle: closeButtonTitle, duration: duration, customize: customize)
        return alert
    }
    
    // Close RPAlertView
    func hideView() {
        if !isHidden {
            isHidden = true
            NSNotificationCenter.defaultCenter().removeObserver(self)
            UIView.animateWithDuration(0.4, animations: {
                self.view.alpha = 0
                }, completion: { finished in
                    self.view.removeFromSuperview()
                    if let event = self.alertViewWillHide {
                        event()
                    }
            })
        }
    }
    
    func keyboardWillShow(){
        UIView.animateWithDuration(0.3, animations: {
            self.view.transform = CGAffineTransformMakeTranslation(0, -140)
            self.view.transform = CGAffineTransformMakeTranslation(0, -140)
        })
    }
    
    func keyboardWillHide(){
        UIView.animateWithDuration(0.3, animations: {
            self.view.transform = CGAffineTransformIdentity
            self.view.transform = CGAffineTransformIdentity
        })
    }
    
}

// ------------------------------------
// Icon drawing
// Code generated by PaintCode
// ------------------------------------

class RPAlertViewStyleKit : NSObject {
    
    // Cache
    struct Cache {
        static var imageOfQuestionmark: UIImage?
        static var questionmarkTargets: [AnyObject]?
        static var imageOfCheckmark: UIImage?
        static var checkmarkTargets: [AnyObject]?
        static var imageOfCross: UIImage?
        static var crossTargets: [AnyObject]?
        static var imageOfNotice: UIImage?
        static var noticeTargets: [AnyObject]?
        static var imageOfWarning: UIImage?
        static var warningTargets: [AnyObject]?
        static var imageOfInfo: UIImage?
        static var infoTargets: [AnyObject]?
        static var imageOfEdit: UIImage?
        static var editTargets: [AnyObject]?
    }
    
    // Drawing Methods
    class func drawQuestionmark() {
        // Questionmark Shape Drawing
        
        //// Group
        //// Bezier 2 Drawing
        let bezier2Path = UIBezierPath()
        bezier2Path.moveToPoint(CGPointMake(44.37, 83.09))
        bezier2Path.addLineToPoint(CGPointMake(32.4, 83.09))
        bezier2Path.addLineToPoint(CGPointMake(32.4, 76.92))
        bezier2Path.addCurveToPoint(CGPointMake(33.51, 69.73), controlPoint1: CGPointMake(32.55, 74.34), controlPoint2: CGPointMake(32.92, 71.94))
        bezier2Path.addCurveToPoint(CGPointMake(36.06, 63.61), controlPoint1: CGPointMake(34.11, 67.52), controlPoint2: CGPointMake(34.96, 65.48))
        bezier2Path.addCurveToPoint(CGPointMake(41.26, 57.46), controlPoint1: CGPointMake(37.16, 61.74), controlPoint2: CGPointMake(38.89, 59.69))
        bezier2Path.addCurveToPoint(CGPointMake(50.25, 50.25), controlPoint1: CGPointMake(43.62, 55.24), controlPoint2: CGPointMake(46.62, 52.83))
        bezier2Path.addCurveToPoint(CGPointMake(67.32, 28.69), controlPoint1: CGPointMake(61.63, 42.24), controlPoint2: CGPointMake(67.32, 35.05))
        bezier2Path.addCurveToPoint(CGPointMake(59.57, 14.35), controlPoint1: CGPointMake(67.32, 23.04), controlPoint2: CGPointMake(64.74, 18.26))
        bezier2Path.addCurveToPoint(CGPointMake(40.79, 8.48), controlPoint1: CGPointMake(54.41, 10.43), controlPoint2: CGPointMake(48.15, 8.48))
        bezier2Path.addCurveToPoint(CGPointMake(12.83, 30.37), controlPoint1: CGPointMake(26.69, 8.48), controlPoint2: CGPointMake(17.37, 15.78))
        bezier2Path.addLineToPoint(CGPointMake(0, 28.8))
        bezier2Path.addCurveToPoint(CGPointMake(41.94, 0), controlPoint1: CGPointMake(5.88, 9.6), controlPoint2: CGPointMake(19.86, 0))
        bezier2Path.addCurveToPoint(CGPointMake(62.16, 3.65), controlPoint1: CGPointMake(49.68, 0), controlPoint2: CGPointMake(56.42, 1.22))
        bezier2Path.addCurveToPoint(CGPointMake(75.56, 13.64), controlPoint1: CGPointMake(67.89, 6.08), controlPoint2: CGPointMake(72.36, 9.41))
        bezier2Path.addCurveToPoint(CGPointMake(80.37, 28.58), controlPoint1: CGPointMake(78.76, 17.87), controlPoint2: CGPointMake(80.37, 22.85))
        bezier2Path.addCurveToPoint(CGPointMake(60.94, 55.86), controlPoint1: CGPointMake(80.37, 37.3), controlPoint2: CGPointMake(73.89, 46.39))
        bezier2Path.addCurveToPoint(CGPointMake(50.9, 64.09), controlPoint1: CGPointMake(56.88, 58.86), controlPoint2: CGPointMake(53.53, 61.6))
        bezier2Path.addCurveToPoint(CGPointMake(45.67, 72.39), controlPoint1: CGPointMake(48.27, 66.58), controlPoint2: CGPointMake(46.53, 69.35))
        bezier2Path.addCurveToPoint(CGPointMake(44.37, 83.09), controlPoint1: CGPointMake(44.8, 75.45), controlPoint2: CGPointMake(44.37, 79.01))
        bezier2Path.closePath()
        bezier2Path.moveToPoint(CGPointMake(47.17, 109.71))
        bezier2Path.addLineToPoint(CGPointMake(30.9, 109.71))
        bezier2Path.addLineToPoint(CGPointMake(30.9, 95.5))
        bezier2Path.addLineToPoint(CGPointMake(47.17, 95.5))
        bezier2Path.addLineToPoint(CGPointMake(47.17, 109.71))
        bezier2Path.closePath()
        bezier2Path.miterLimit = 4;
        
        UIColor.whiteColor().setFill()
        bezier2Path.fill()
        
        
    }
    
    class func drawCheckmark() {
        // Checkmark Shape Drawing
        let checkmarkShapePath = UIBezierPath()
        checkmarkShapePath.moveToPoint(CGPointMake(73.25, 14.05))
        checkmarkShapePath.addCurveToPoint(CGPointMake(64.51, 13.86), controlPoint1: CGPointMake(70.98, 11.44), controlPoint2: CGPointMake(66.78, 11.26))
        checkmarkShapePath.addLineToPoint(CGPointMake(27.46, 52))
        checkmarkShapePath.addLineToPoint(CGPointMake(15.75, 39.54))
        checkmarkShapePath.addCurveToPoint(CGPointMake(6.84, 39.54), controlPoint1: CGPointMake(13.48, 36.93), controlPoint2: CGPointMake(9.28, 36.93))
        checkmarkShapePath.addCurveToPoint(CGPointMake(6.84, 49.02), controlPoint1: CGPointMake(4.39, 42.14), controlPoint2: CGPointMake(4.39, 46.42))
        checkmarkShapePath.addLineToPoint(CGPointMake(22.91, 66.14))
        checkmarkShapePath.addCurveToPoint(CGPointMake(27.28, 68), controlPoint1: CGPointMake(24.14, 67.44), controlPoint2: CGPointMake(25.71, 68))
        checkmarkShapePath.addCurveToPoint(CGPointMake(31.65, 66.14), controlPoint1: CGPointMake(28.86, 68), controlPoint2: CGPointMake(30.43, 67.26))
        checkmarkShapePath.addLineToPoint(CGPointMake(73.08, 23.35))
        checkmarkShapePath.addCurveToPoint(CGPointMake(73.25, 14.05), controlPoint1: CGPointMake(75.52, 20.75), controlPoint2: CGPointMake(75.7, 16.65))
        checkmarkShapePath.closePath()
        checkmarkShapePath.miterLimit = 4;
        
        UIColor.whiteColor().setFill()
        checkmarkShapePath.fill()
    }
    
    class func drawCross() {
        // Cross Shape Drawing
        let crossShapePath = UIBezierPath()
        crossShapePath.moveToPoint(CGPointMake(10, 70))
        crossShapePath.addLineToPoint(CGPointMake(70, 10))
        crossShapePath.moveToPoint(CGPointMake(10, 10))
        crossShapePath.addLineToPoint(CGPointMake(70, 70))
        crossShapePath.lineCapStyle = CGLineCap.Round;
        crossShapePath.lineJoinStyle = CGLineJoin.Round;
        UIColor.whiteColor().setStroke()
        crossShapePath.lineWidth = 14
        crossShapePath.stroke()
    }
    
    class func drawNotice() {
        // Notice Shape Drawing
        let noticeShapePath = UIBezierPath()
        noticeShapePath.moveToPoint(CGPointMake(72, 48.54))
        noticeShapePath.addLineToPoint(CGPointMake(72, 39.9))
        noticeShapePath.addCurveToPoint(CGPointMake(66.38, 34.01), controlPoint1: CGPointMake(72, 36.76), controlPoint2: CGPointMake(69.48, 34.01))
        noticeShapePath.addCurveToPoint(CGPointMake(61.53, 35.97), controlPoint1: CGPointMake(64.82, 34.01), controlPoint2: CGPointMake(62.69, 34.8))
        noticeShapePath.addCurveToPoint(CGPointMake(60.36, 35.78), controlPoint1: CGPointMake(61.33, 35.97), controlPoint2: CGPointMake(62.3, 35.78))
        noticeShapePath.addLineToPoint(CGPointMake(60.36, 33.22))
        noticeShapePath.addCurveToPoint(CGPointMake(54.16, 26.16), controlPoint1: CGPointMake(60.36, 29.3), controlPoint2: CGPointMake(57.65, 26.16))
        noticeShapePath.addCurveToPoint(CGPointMake(48.73, 29.89), controlPoint1: CGPointMake(51.64, 26.16), controlPoint2: CGPointMake(50.67, 27.73))
        noticeShapePath.addLineToPoint(CGPointMake(48.73, 28.71))
        noticeShapePath.addCurveToPoint(CGPointMake(43.49, 21.64), controlPoint1: CGPointMake(48.73, 24.78), controlPoint2: CGPointMake(46.98, 21.64))
        noticeShapePath.addCurveToPoint(CGPointMake(39.03, 25.37), controlPoint1: CGPointMake(40.97, 21.64), controlPoint2: CGPointMake(39.03, 23.01))
        noticeShapePath.addLineToPoint(CGPointMake(39.03, 9.07))
        noticeShapePath.addCurveToPoint(CGPointMake(32.24, 2), controlPoint1: CGPointMake(39.03, 5.14), controlPoint2: CGPointMake(35.73, 2))
        noticeShapePath.addCurveToPoint(CGPointMake(25.45, 9.07), controlPoint1: CGPointMake(28.56, 2), controlPoint2: CGPointMake(25.45, 5.14))
        noticeShapePath.addLineToPoint(CGPointMake(25.45, 41.47))
        noticeShapePath.addCurveToPoint(CGPointMake(24.29, 43.44), controlPoint1: CGPointMake(25.45, 42.45), controlPoint2: CGPointMake(24.68, 43.04))
        noticeShapePath.addCurveToPoint(CGPointMake(9.55, 43.04), controlPoint1: CGPointMake(16.73, 40.88), controlPoint2: CGPointMake(11.88, 40.69))
        noticeShapePath.addCurveToPoint(CGPointMake(8, 46.58), controlPoint1: CGPointMake(8.58, 43.83), controlPoint2: CGPointMake(8, 45.2))
        noticeShapePath.addCurveToPoint(CGPointMake(14.4, 55.81), controlPoint1: CGPointMake(8.19, 50.31), controlPoint2: CGPointMake(12.07, 53.84))
        noticeShapePath.addLineToPoint(CGPointMake(27.2, 69.56))
        noticeShapePath.addCurveToPoint(CGPointMake(42.91, 77.8), controlPoint1: CGPointMake(30.5, 74.47), controlPoint2: CGPointMake(35.73, 77.21))
        noticeShapePath.addCurveToPoint(CGPointMake(43.88, 77.8), controlPoint1: CGPointMake(43.3, 77.8), controlPoint2: CGPointMake(43.68, 77.8))
        noticeShapePath.addCurveToPoint(CGPointMake(47.18, 78), controlPoint1: CGPointMake(45.04, 77.8), controlPoint2: CGPointMake(46.01, 78))
        noticeShapePath.addLineToPoint(CGPointMake(48.34, 78))
        noticeShapePath.addLineToPoint(CGPointMake(48.34, 78))
        noticeShapePath.addCurveToPoint(CGPointMake(71.61, 52.08), controlPoint1: CGPointMake(56.48, 78), controlPoint2: CGPointMake(69.87, 75.05))
        noticeShapePath.addCurveToPoint(CGPointMake(72, 48.54), controlPoint1: CGPointMake(71.81, 51.29), controlPoint2: CGPointMake(72, 49.72))
        noticeShapePath.closePath()
        noticeShapePath.miterLimit = 4;
        
        UIColor.whiteColor().setFill()
        noticeShapePath.fill()
    }
    
    class func drawWarning() {
        // Color Declarations
        let greyColor = UIColor(red: 0.236, green: 0.236, blue: 0.236, alpha: 1.000)
        
        // Warning Group
        // Warning Circle Drawing
        let warningCirclePath = UIBezierPath()
        warningCirclePath.moveToPoint(CGPointMake(40.94, 63.39))
        warningCirclePath.addCurveToPoint(CGPointMake(36.03, 65.55), controlPoint1: CGPointMake(39.06, 63.39), controlPoint2: CGPointMake(37.36, 64.18))
        warningCirclePath.addCurveToPoint(CGPointMake(34.14, 70.45), controlPoint1: CGPointMake(34.9, 66.92), controlPoint2: CGPointMake(34.14, 68.49))
        warningCirclePath.addCurveToPoint(CGPointMake(36.22, 75.54), controlPoint1: CGPointMake(34.14, 72.41), controlPoint2: CGPointMake(34.9, 74.17))
        warningCirclePath.addCurveToPoint(CGPointMake(40.94, 77.5), controlPoint1: CGPointMake(37.54, 76.91), controlPoint2: CGPointMake(39.06, 77.5))
        warningCirclePath.addCurveToPoint(CGPointMake(45.86, 75.35), controlPoint1: CGPointMake(42.83, 77.5), controlPoint2: CGPointMake(44.53, 76.72))
        warningCirclePath.addCurveToPoint(CGPointMake(47.93, 70.45), controlPoint1: CGPointMake(47.18, 74.17), controlPoint2: CGPointMake(47.93, 72.41))
        warningCirclePath.addCurveToPoint(CGPointMake(45.86, 65.35), controlPoint1: CGPointMake(47.93, 68.49), controlPoint2: CGPointMake(47.18, 66.72))
        warningCirclePath.addCurveToPoint(CGPointMake(40.94, 63.39), controlPoint1: CGPointMake(44.53, 64.18), controlPoint2: CGPointMake(42.83, 63.39))
        warningCirclePath.closePath()
        warningCirclePath.miterLimit = 4;
        
        greyColor.setFill()
        warningCirclePath.fill()
        
        
        // Warning Shape Drawing
        let warningShapePath = UIBezierPath()
        warningShapePath.moveToPoint(CGPointMake(46.23, 4.26))
        warningShapePath.addCurveToPoint(CGPointMake(40.94, 2.5), controlPoint1: CGPointMake(44.91, 3.09), controlPoint2: CGPointMake(43.02, 2.5))
        warningShapePath.addCurveToPoint(CGPointMake(34.71, 4.26), controlPoint1: CGPointMake(38.68, 2.5), controlPoint2: CGPointMake(36.03, 3.09))
        warningShapePath.addCurveToPoint(CGPointMake(31.5, 8.77), controlPoint1: CGPointMake(33.01, 5.44), controlPoint2: CGPointMake(31.5, 7.01))
        warningShapePath.addLineToPoint(CGPointMake(31.5, 19.36))
        warningShapePath.addLineToPoint(CGPointMake(34.71, 54.44))
        warningShapePath.addCurveToPoint(CGPointMake(40.38, 58.16), controlPoint1: CGPointMake(34.9, 56.2), controlPoint2: CGPointMake(36.41, 58.16))
        warningShapePath.addCurveToPoint(CGPointMake(45.67, 54.44), controlPoint1: CGPointMake(44.34, 58.16), controlPoint2: CGPointMake(45.67, 56.01))
        warningShapePath.addLineToPoint(CGPointMake(48.5, 19.36))
        warningShapePath.addLineToPoint(CGPointMake(48.5, 8.77))
        warningShapePath.addCurveToPoint(CGPointMake(46.23, 4.26), controlPoint1: CGPointMake(48.5, 7.01), controlPoint2: CGPointMake(47.74, 5.44))
        warningShapePath.closePath()
        warningShapePath.miterLimit = 4;
        
        greyColor.setFill()
        warningShapePath.fill()
    }
    
    class func drawInfo() {
        // Color Declarations
        let color0 = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)
        
        // Info Shape Drawing
        let infoShapePath = UIBezierPath()
        infoShapePath.moveToPoint(CGPointMake(45.66, 15.96))
        infoShapePath.addCurveToPoint(CGPointMake(45.66, 5.22), controlPoint1: CGPointMake(48.78, 12.99), controlPoint2: CGPointMake(48.78, 8.19))
        infoShapePath.addCurveToPoint(CGPointMake(34.34, 5.22), controlPoint1: CGPointMake(42.53, 2.26), controlPoint2: CGPointMake(37.47, 2.26))
        infoShapePath.addCurveToPoint(CGPointMake(34.34, 15.96), controlPoint1: CGPointMake(31.22, 8.19), controlPoint2: CGPointMake(31.22, 12.99))
        infoShapePath.addCurveToPoint(CGPointMake(45.66, 15.96), controlPoint1: CGPointMake(37.47, 18.92), controlPoint2: CGPointMake(42.53, 18.92))
        infoShapePath.closePath()
        infoShapePath.moveToPoint(CGPointMake(48, 69.41))
        infoShapePath.addCurveToPoint(CGPointMake(40, 77), controlPoint1: CGPointMake(48, 73.58), controlPoint2: CGPointMake(44.4, 77))
        infoShapePath.addLineToPoint(CGPointMake(40, 77))
        infoShapePath.addCurveToPoint(CGPointMake(32, 69.41), controlPoint1: CGPointMake(35.6, 77), controlPoint2: CGPointMake(32, 73.58))
        infoShapePath.addLineToPoint(CGPointMake(32, 35.26))
        infoShapePath.addCurveToPoint(CGPointMake(40, 27.67), controlPoint1: CGPointMake(32, 31.08), controlPoint2: CGPointMake(35.6, 27.67))
        infoShapePath.addLineToPoint(CGPointMake(40, 27.67))
        infoShapePath.addCurveToPoint(CGPointMake(48, 35.26), controlPoint1: CGPointMake(44.4, 27.67), controlPoint2: CGPointMake(48, 31.08))
        infoShapePath.addLineToPoint(CGPointMake(48, 69.41))
        infoShapePath.closePath()
        color0.setFill()
        infoShapePath.fill()
    }
    
    class func drawEdit() {
        // Color Declarations
        let color = UIColor(red:1.0, green:1.0, blue:1.0, alpha:1.0)
        
        // Edit shape Drawing
        let editPathPath = UIBezierPath()
        editPathPath.moveToPoint(CGPointMake(71, 2.7))
        editPathPath.addCurveToPoint(CGPointMake(71.9, 15.2), controlPoint1: CGPointMake(74.7, 5.9), controlPoint2: CGPointMake(75.1, 11.6))
        editPathPath.addLineToPoint(CGPointMake(64.5, 23.7))
        editPathPath.addLineToPoint(CGPointMake(49.9, 11.1))
        editPathPath.addLineToPoint(CGPointMake(57.3, 2.6))
        editPathPath.addCurveToPoint(CGPointMake(69.7, 1.7), controlPoint1: CGPointMake(60.4, -1.1), controlPoint2: CGPointMake(66.1, -1.5))
        editPathPath.addLineToPoint(CGPointMake(71, 2.7))
        editPathPath.addLineToPoint(CGPointMake(71, 2.7))
        editPathPath.closePath()
        editPathPath.moveToPoint(CGPointMake(47.8, 13.5))
        editPathPath.addLineToPoint(CGPointMake(13.4, 53.1))
        editPathPath.addLineToPoint(CGPointMake(15.7, 55.1))
        editPathPath.addLineToPoint(CGPointMake(50.1, 15.5))
        editPathPath.addLineToPoint(CGPointMake(47.8, 13.5))
        editPathPath.addLineToPoint(CGPointMake(47.8, 13.5))
        editPathPath.closePath()
        editPathPath.moveToPoint(CGPointMake(17.7, 56.7))
        editPathPath.addLineToPoint(CGPointMake(23.8, 62.2))
        editPathPath.addLineToPoint(CGPointMake(58.2, 22.6))
        editPathPath.addLineToPoint(CGPointMake(52, 17.1))
        editPathPath.addLineToPoint(CGPointMake(17.7, 56.7))
        editPathPath.addLineToPoint(CGPointMake(17.7, 56.7))
        editPathPath.closePath()
        editPathPath.moveToPoint(CGPointMake(25.8, 63.8))
        editPathPath.addLineToPoint(CGPointMake(60.1, 24.2))
        editPathPath.addLineToPoint(CGPointMake(62.3, 26.1))
        editPathPath.addLineToPoint(CGPointMake(28.1, 65.7))
        editPathPath.addLineToPoint(CGPointMake(25.8, 63.8))
        editPathPath.addLineToPoint(CGPointMake(25.8, 63.8))
        editPathPath.closePath()
        editPathPath.moveToPoint(CGPointMake(25.9, 68.1))
        editPathPath.addLineToPoint(CGPointMake(4.2, 79.5))
        editPathPath.addLineToPoint(CGPointMake(11.3, 55.5))
        editPathPath.addLineToPoint(CGPointMake(25.9, 68.1))
        editPathPath.closePath()
        editPathPath.miterLimit = 4;
        editPathPath.usesEvenOddFillRule = true;
        color.setFill()
        editPathPath.fill()
    }
    
    // Generated Images
    class var imageOfQuestionmark: UIImage {
        if (Cache.imageOfQuestionmark != nil) {
            return Cache.imageOfQuestionmark!
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 120), false, 0)
        RPAlertViewStyleKit.drawQuestionmark()
        Cache.imageOfQuestionmark = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfQuestionmark!
    }
    
    class var imageOfCheckmark: UIImage {
        if (Cache.imageOfCheckmark != nil) {
            return Cache.imageOfCheckmark!
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), false, 0)
        RPAlertViewStyleKit.drawCheckmark()
        Cache.imageOfCheckmark = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfCheckmark!
    }
    
    class var imageOfCross: UIImage {
        if (Cache.imageOfCross != nil) {
            return Cache.imageOfCross!
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), false, 0)
        RPAlertViewStyleKit.drawCross()
        Cache.imageOfCross = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfCross!
    }
    
    class var imageOfNotice: UIImage {
        if (Cache.imageOfNotice != nil) {
            return Cache.imageOfNotice!
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), false, 0)
        RPAlertViewStyleKit.drawNotice()
        Cache.imageOfNotice = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfNotice!
    }
    
    class var imageOfWarning: UIImage {
        if (Cache.imageOfWarning != nil) {
            return Cache.imageOfWarning!
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), false, 0)
        RPAlertViewStyleKit.drawWarning()
        Cache.imageOfWarning = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfWarning!
    }
    
    class var imageOfInfo: UIImage {
        if (Cache.imageOfInfo != nil) {
            return Cache.imageOfInfo!
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), false, 0)
        RPAlertViewStyleKit.drawInfo()
        Cache.imageOfInfo = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfInfo!
    }
    
    class var imageOfEdit: UIImage {
        if (Cache.imageOfEdit != nil) {
            return Cache.imageOfEdit!
        }
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), false, 0)
        RPAlertViewStyleKit.drawEdit()
        Cache.imageOfEdit = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return Cache.imageOfEdit!
    }
}
