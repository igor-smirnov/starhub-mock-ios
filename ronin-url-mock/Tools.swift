//
//  Tools.swift
//  ronin-url-mock
//
//  Created by Igor Smirnov on 18/02/16.
//  Copyright © 2016 Ronin Project OOO. All rights reserved.
//

import UIKit

var currentApplicationIndex: (appIndex: Int, kindIndex: Int) = (appIndex: 0, kindIndex: 0)

enum ApplicationKind {
    case Release
    case Beta
    var stringValue: String {
        switch self {
        case .Release: return "Release"
        case .Beta: return "Beta"
        }
    }
}

typealias ApplicationItem = (kind: ApplicationKind, urlScheme: String)
typealias ApplicationInfo = (appName: String, items: [ApplicationItem])

let Applications: [ApplicationInfo] = [
    (appName: "Ronin",          items: [(kind: .Release, urlScheme: "ronin"         ), (kind: .Beta, urlScheme: "roninbeta"         )]),
    (appName: "Seven Sky",      items: [(kind: .Release, urlScheme: "sevensky"      ), (kind: .Beta, urlScheme: "sevenskybeta"      )]),
    (appName: "Cifra 1",        items: [(kind: .Release, urlScheme: "cifra1"        ), (kind: .Beta, urlScheme: "cifra1beta"        )]),
    (appName: "NetByNet",       items: [(kind: .Release, urlScheme: "netbynet"      ), (kind: .Beta, urlScheme: "netbynetbeta"      )]),
    (appName: "Jurong Point",   items: [(kind: .Release, urlScheme: "jurongpoint"   ), (kind: .Beta, urlScheme: "jurongpointbeta"   )]),
    (appName: "Groupon",        items: [(kind: .Release, urlScheme: "grpnrnn"       ), (kind: .Beta, urlScheme: "grpnrnnbeta"       )]),
    (appName: "Tele 2",         items: [(kind: .Release, urlScheme: "tele2rlc"      ), (kind: .Beta, urlScheme: "tele2rlcbeta"      )]),
    (appName: "MTS",            items: [(kind: .Release, urlScheme: "mtsrlc"        ), (kind: .Beta, urlScheme: "mtsrlcbeta"        )]),
    (appName: "StarHub",        items: [(kind: .Release, urlScheme: "shbonus"       ), (kind: .Beta, urlScheme: "shbonusbeta"       )]),
    (appName: "Rostelecom",     items: [(kind: .Release, urlScheme: "rtrlc"         ), (kind: .Beta, urlScheme: "rtrlcbeta"         )])
]

extension NSURL {

    func errorOpenURL() {
        
        RPAlertView.show(
            style: RPAlertViewStyle.Error,
            title: "Error",
            subTitle: "Could not open URL:\n\(absoluteString)",
            closeButtonTitle: "Cancel"
        )
        
    }

    func showURLWithTitle(title: String, info: String? = nil, perform: (() -> Void)? = nil) {
        let text = (info == nil ? "" : "\(info!) ") + "\nURL: \(absoluteString)"
        if let closure = perform {
            RPAlertView.show(
                style: RPAlertViewStyle.Info,
                title: title,
                subTitle: text,
                closeButtonTitle: "Cancel"
                ) { alert in
                    alert.addButton("Perform", action: closure)
            }
        } else {
            RPAlertView.show(
                style: RPAlertViewStyle.Info,
                title: title,
                subTitle: text,
                closeButtonTitle: "Close"
            )
        }
        
    }

    class func makeURLWithScheme(scheme: String, host: String, path: String?, query: [String: String]) -> NSURL? {
        let urlComponents = NSURLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = query.map { return NSURLQueryItem(name: $0, value: $1) }
        return urlComponents.URL
    }
    

}

